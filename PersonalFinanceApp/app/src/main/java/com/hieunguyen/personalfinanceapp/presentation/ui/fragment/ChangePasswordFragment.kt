package com.hieunguyen.personalfinanceapp.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.hieunguyen.personalfinanceapp.databinding.FragmentChangePasswordBinding
import com.hieunguyen.personalfinanceapp.databinding.FragmentTodayBinding

class ChangePasswordFragment: Fragment() {
    private lateinit var binding: FragmentChangePasswordBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChangePasswordBinding.inflate(inflater, container, false)
        return binding.root
    }
}