package com.hieunguyen.personalfinanceapp.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.hieunguyen.personalfinanceapp.databinding.FragmentMonthAnalyticsBinding
import com.hieunguyen.personalfinanceapp.databinding.FragmentTodayBinding

class MonthAnalyticsFragment: Fragment() {
    private lateinit var binding: FragmentMonthAnalyticsBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMonthAnalyticsBinding.inflate(inflater, container, false)
        return binding.root
    }
}