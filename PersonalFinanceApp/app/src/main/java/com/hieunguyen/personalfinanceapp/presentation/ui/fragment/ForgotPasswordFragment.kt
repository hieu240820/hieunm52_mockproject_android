package com.hieunguyen.personalfinanceapp.presentation.ui.fragment

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.hieunguyen.personalfinanceapp.databinding.FragmentForgotPasswordBinding

class ForgotPasswordFragment: Fragment() {
    private lateinit var binding: FragmentForgotPasswordBinding
    private lateinit var progressDialog: ProgressDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentForgotPasswordBinding.inflate(inflater, container, false)
        progressDialog = ProgressDialog(requireContext())
        initOnClick()
        return binding.root
    }
    private fun initOnClick() {
        binding.btnSendCode.setOnClickListener {
            progressDialog.show()
            progressDialog.setMessage("Sending...")
            val auth = FirebaseAuth.getInstance()
            val email = binding.edtEmail.text.toString().trim()
            auth.sendPasswordResetEmail(email).addOnCompleteListener { task ->
                progressDialog.dismiss()
                if (task.isSuccessful) {
                    Toast.makeText(requireContext(), "Email sent", Toast.LENGTH_SHORT).show()
                }
            }
        }
            }
        }