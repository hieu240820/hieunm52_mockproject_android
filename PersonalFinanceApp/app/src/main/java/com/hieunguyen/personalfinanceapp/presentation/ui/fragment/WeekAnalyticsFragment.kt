package com.hieunguyen.personalfinanceapp.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.hieunguyen.personalfinanceapp.databinding.FragmentTodayBinding
import com.hieunguyen.personalfinanceapp.databinding.FragmentWeekAnalyticsBinding

class WeekAnalyticsFragment: Fragment() {
    private lateinit var binding: FragmentWeekAnalyticsBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWeekAnalyticsBinding.inflate(inflater, container, false)
        return binding.root
    }
}