package com.hieunguyen.personalfinanceapp.presentation.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.navigation.NavigationView
import com.hieunguyen.personalfinanceapp.R
import com.hieunguyen.personalfinanceapp.databinding.ActivityMainBinding
import com.hieunguyen.personalfinanceapp.presentation.ui.fragment.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener  {
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private var drawerLayout: DrawerLayout? = null
    private var navigationView: NavigationView? = null
    private val FRAGMENT_MY_BUDGET = 0
    private val FRAGMENT_TODAY = 1
    private val FRAGMENT_WEEK = 2
    private val FRAGMENT_MONTH = 3
    private val FRAGMENT_HISTORY = 5
    private val FRAGMENT_PROFILE = 6
    private val FRAGMENT_CHANGE_PASSWORD = 7
    private val FRAGMENT_TODAY_ANALYTICS = 8
    private val FRAGMENT_WEEK_ANALYTICS = 9
    private val FRAGMENT_MONTH_ANALYTICS = 10
    private val FRAGMENT_TOTAL_BUDGET = 11
    private var mCurrentFragment: Int = FRAGMENT_TOTAL_BUDGET


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        drawerLayout = binding.drawerLayout
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(null)
        val toggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.navi_draw_open,
            R.string.navi_draw_close
        )
        drawerLayout!!.addDrawerListener(toggle)
        toggle.syncState()

        navigationView = binding.navigationview
        navigationView!!.setNavigationItemSelectedListener(this)


        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.fragment_container) as NavHostFragment

        navController = navHostFragment.navController

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.navi_today_budget) {
            if (mCurrentFragment != FRAGMENT_TOTAL_BUDGET) {
                replaceFragment(TotalBudgetFragment())
                mCurrentFragment = FRAGMENT_TOTAL_BUDGET
            }
        } else if (id == R.id.navi_my_budget) {
            if (mCurrentFragment != FRAGMENT_MY_BUDGET) {
                replaceFragment(MyBudgetFragment())
                mCurrentFragment = FRAGMENT_MY_BUDGET
            }
        } else if (id == R.id.navi_today) {
            if (mCurrentFragment != FRAGMENT_TODAY) {
                replaceFragment(TodayFragment())
                mCurrentFragment = FRAGMENT_TODAY
            }
        } else if (id == R.id.navi_week) {
            if (mCurrentFragment != FRAGMENT_WEEK) {
                replaceFragment(WeekFragment())
                mCurrentFragment = FRAGMENT_WEEK
            }
        } else if (id == R.id.navi_month) {
            if (mCurrentFragment != FRAGMENT_MONTH) {
                replaceFragment(MonthFragment())
                mCurrentFragment = FRAGMENT_MONTH
            }
        } else if (id == R.id.navi_history) {
            if (mCurrentFragment != FRAGMENT_HISTORY) {
                replaceFragment(HistoryFragment())
                mCurrentFragment = FRAGMENT_HISTORY
            }
        } else if (id == R.id.navi_profile) {
            if (mCurrentFragment != FRAGMENT_PROFILE) {
                replaceFragment(MyProfileFragment())
                mCurrentFragment = FRAGMENT_PROFILE
            }
        } else if (id == R.id.navi_change_password) {
            if (mCurrentFragment != FRAGMENT_CHANGE_PASSWORD) {
                replaceFragment(ChangePasswordFragment())
                mCurrentFragment = FRAGMENT_CHANGE_PASSWORD
            }
        } else if (id == R.id.analystics_today) {
            if (mCurrentFragment != FRAGMENT_TODAY_ANALYTICS) {
                replaceFragment(TodayAnalyticsFragment())
                mCurrentFragment = FRAGMENT_TODAY_ANALYTICS
            }
        } else if (id == R.id.analystics_week) {
            if (mCurrentFragment != FRAGMENT_WEEK_ANALYTICS) {
                replaceFragment(WeekAnalyticsFragment())
                mCurrentFragment = FRAGMENT_WEEK_ANALYTICS
            }
        } else if (id == R.id.analystics_month) {
            if (mCurrentFragment != FRAGMENT_MONTH_ANALYTICS) {
                replaceFragment(MonthAnalyticsFragment())
                mCurrentFragment = FRAGMENT_MONTH_ANALYTICS
            }
        }

        drawerLayout!!.closeDrawer(GravityCompat.START)

        return true
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.commit {
            replace(R.id.fragment_container, fragment)
        }
    }

    override fun onBackPressed() {
        if (drawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            drawerLayout!!.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

}