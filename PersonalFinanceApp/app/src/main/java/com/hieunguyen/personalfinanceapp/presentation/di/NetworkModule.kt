package com.hieunguyen.personalfinanceapp.presentation.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.hieunguyen.personalfinanceapp.data.basenetwork.UserDataSource
import com.hieunguyen.personalfinanceapp.data.basenetwork.UserDataSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideFirebaseAuth(): FirebaseAuth {
        return Firebase.auth
    }

    @Provides
    @Singleton
    fun provideUserDataSource(firebaseAuth: FirebaseAuth): UserDataSource {
        return UserDataSourceImpl(firebaseAuth)
    }
}