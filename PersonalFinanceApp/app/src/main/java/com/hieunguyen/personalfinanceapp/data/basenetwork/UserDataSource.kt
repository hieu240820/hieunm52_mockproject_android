package com.hieunguyen.personalfinanceapp.data.basenetwork

interface UserDataSource {

    suspend fun login(email: String, password: String): Boolean

    //login
    suspend fun create(email: String, password: String): Boolean
}