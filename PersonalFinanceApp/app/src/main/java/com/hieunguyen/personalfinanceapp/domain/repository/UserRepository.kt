package com.hieunguyen.personalfinanceapp.domain.repository

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.hieunguyen.personalfinanceapp.domain.FireBaseState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

 interface UserRepository {
     suspend fun login(email: String, password: String): Flow<FireBaseState<Boolean>>
     suspend fun create(email: String, password: String):  Flow<FireBaseState<Boolean>>
 }


