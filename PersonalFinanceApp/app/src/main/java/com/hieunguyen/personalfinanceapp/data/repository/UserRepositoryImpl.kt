package com.hieunguyen.personalfinanceapp.data.repository

import com.google.android.gms.tasks.Tasks.await
import com.google.firebase.auth.FirebaseAuth
import com.hieunguyen.personalfinanceapp.data.basenetwork.UserDataSource
import com.hieunguyen.personalfinanceapp.domain.FireBaseState
import com.hieunguyen.personalfinanceapp.domain.repository.UserRepository
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await

class UserRepositoryImpl(): UserRepository {

    override suspend fun login(email: String, password: String): Flow<FireBaseState<Boolean>> = callbackFlow {
        trySend(FireBaseState.Loading(""))
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            .addOnFailureListener {
                trySend(FireBaseState.Fail(it.message.toString()))
            }
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    trySend(FireBaseState.Success(true))
                }
            }
            .await()

        awaitClose()
    }

    override suspend fun create(email: String, password: String):Flow<FireBaseState<Boolean>> = callbackFlow{
    }
}