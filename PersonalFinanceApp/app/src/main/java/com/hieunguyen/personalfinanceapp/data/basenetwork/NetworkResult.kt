package com.hieunguyen.personalfinanceapp.data.basenetwork

sealed class NetworkResult<out T: Any> {
    data class Sucess<out T:Any>(val data : T) : NetworkResult<T>()
    data class Error(val exception: Exception) : NetworkResult<Nothing>()
}