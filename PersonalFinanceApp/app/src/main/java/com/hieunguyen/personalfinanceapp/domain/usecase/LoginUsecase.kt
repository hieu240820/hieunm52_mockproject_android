package com.hieunguyen.personalfinanceapp.domain.usecase

import com.hieunguyen.personalfinanceapp.domain.repository.UserRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LoginUseCase @Inject constructor(private val userRepository: UserRepository) {
     suspend operator fun invoke(email : String, password : String ) = userRepository.login(email, password)
}