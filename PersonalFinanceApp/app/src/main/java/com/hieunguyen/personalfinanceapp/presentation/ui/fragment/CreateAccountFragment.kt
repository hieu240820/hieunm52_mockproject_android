package com.hieunguyen.personalfinanceapp.presentation.ui.fragment

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.hieunguyen.personalfinanceapp.R
import com.hieunguyen.personalfinanceapp.databinding.FragmentCreateAccountBinding
import com.hieunguyen.personalfinanceapp.databinding.FragmentLoginBinding
class CreateAccountFragment : Fragment() {

    private lateinit var binding: FragmentCreateAccountBinding
    private lateinit var auth: FirebaseAuth
    private lateinit var progressDialog: ProgressDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCreateAccountBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        auth = FirebaseAuth.getInstance()
        progressDialog = ProgressDialog(requireContext())

        binding.btnCreate.setOnClickListener {
            onClickCreate()
        }

        binding.linearLayoutLogin.setOnClickListener {
            navigateToLogin()
        }
    }

    private fun onClickCreate() {
        val email = binding.edtCreateEmail.text.toString().trim()
        val password = binding.edtCreatePassword.text.toString().trim()

        if (TextUtils.isEmpty(email)) {
            binding.edtCreateEmail.error = "Email is required"
            return
        }

        if (TextUtils.isEmpty(password)) {
            binding.edtCreatePassword.error = "Password is required"
            return
        }
        progressDialog.setMessage("Creation in progress")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    navigateToLogin()
                    progressDialog.dismiss()
                    Toast.makeText(requireContext(), "Create Account Succesfully",Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(requireContext(), "Failed", Toast.LENGTH_SHORT).show()
                    progressDialog.dismiss()
                }
            }
    }

    private fun navigateToLogin() {
        findNavController().navigate(R.id.action_createAccountFragment_to_loginFragment)
    }
}


