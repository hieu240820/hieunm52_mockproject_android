package com.hieunguyen.personalfinanceapp.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.hieunguyen.personalfinanceapp.databinding.FragmentMyBudgetBinding
import com.hieunguyen.personalfinanceapp.databinding.FragmentTotalBudgetBinding

class TotalBudgetFragment: Fragment() {
    private lateinit var binding: FragmentTotalBudgetBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTotalBudgetBinding.inflate(inflater, container, false)
        return binding.root
    }
}