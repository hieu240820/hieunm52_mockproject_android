package com.hieunguyen.personalfinanceapp.presentation.ui.fragment

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.hieunguyen.personalfinanceapp.R
import com.hieunguyen.personalfinanceapp.databinding.FragmentLoginBinding
import com.hieunguyen.personalfinanceapp.domain.FireBaseState
import com.hieunguyen.personalfinanceapp.presentation.base.BaseFragment
import com.hieunguyen.personalfinanceapp.presentation.model.LoginViewModel
import com.hieunguyen.personalfinanceapp.presentation.ui.MainActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding>(FragmentLoginBinding::inflate){


    private val viewModel: LoginViewModel by viewModels()

    override fun bindView() {
        super.bindView()
        with(binding) {
            //btnLogin.setOnClickListener(this@LoginFragment)
        }
    }

    override fun observeViewModel() {
        super.observeViewModel()
        lifecycleScope.launch {
            viewModel.loginState.collectLatest {
                loginUser(it)
            }
        }
    }

    private fun loginUser(state: FireBaseState<String>) {
//        when (state) {
//            is FireBaseState.Success -> {
//                Toast.makeText(requireContext(), "Login Success", Toast.LENGTH_SHORT).show()
//            }
//
//            is FireBaseState.Fail -> {
//                Toast.makeText(requireContext(), state.msg.toString(), Toast.LENGTH_SHORT)
//                    .show()
//            }
        }
    }

//    override fun onClick(view: View?) {
//        with(binding) {
//            when (view) {
//                btnLogin -> {
//                    viewModel.login(edtEmail.text.toString(), edtPassword.text.toString())
//                    findNavController().navigate(
//                        R.id.action_login_to_home,
//                        null
//                    )
//                }
//            }
//        }
//    }

//    private lateinit var progressDialog: ProgressDialog
//    private lateinit var mAuth: FirebaseAuth
//    private val viewModel: LoginViewModel by viewModels()
//
//
//    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        initOnClick()
//        mAuth = FirebaseAuth.getInstance()
//        progressDialog = ProgressDialog(requireContext())
//        return binding.root
//    }
//
//    private fun initOnClick() {
//        binding.btnLogin.setOnClickListener {
//            val email = binding.edtEmail.text.toString().trim()
//            val password = binding.edtPassword.text.toString().trim()
//            if (TextUtils.isEmpty(email)) {
//                binding.edtEmail.error = "Email is required"
//            }
//            if (TextUtils.isEmpty(password)) {
//                binding.edtPassword.error = "Password is required"
//            } else {
//                progressDialog.setMessage("Login in progress")
//                progressDialog.setCanceledOnTouchOutside(false)
//                progressDialog.show()
//
//                mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
//                    if (task.isSuccessful) {
//                       findNavController().navigate(R.id.action_login_to_home)
//                        progressDialog.dismiss()
//                    } else {
//                        Toast.makeText(requireContext(), "Failed", Toast.LENGTH_SHORT).show()
//                        progressDialog.dismiss()
//                    }
//                }
//            }
//        }
//        binding.txtCreateNewPass.setOnClickListener {
//            findNavController().navigate(R.id.action_loginFragment_to_createAccountFragment)
//        }
//        binding.txtForgotPassword.setOnClickListener {
//            findNavController().navigate(R.id.action_loginFragment_to_forgotPasswordFragment)
//        }
//    }


