package com.hieunguyen.personalfinanceapp.domain

sealed class FireBaseState<T>(
    val data: T? = null,
    val msg: String? = null
) {
    class Success<T>(data: T?) : FireBaseState<T>(data)
    class Fail<T>(msg: String?) : FireBaseState<T>(msg = msg)
    class Loading<T>(msg : String) : FireBaseState<T>(msg = msg)
}