package com.hieunguyen.personalfinanceapp.presentation.model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hieunguyen.personalfinanceapp.domain.FireBaseState
import com.hieunguyen.personalfinanceapp.domain.usecase.LoginUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class LoginViewModel @Inject constructor(private val usercase: LoginUseCase) : ViewModel() {

    private val _loginState = MutableSharedFlow<FireBaseState<String>>()
    val loginState : SharedFlow<FireBaseState<String>> get() = _loginState

    fun login(email: String, password: String){
        viewModelScope.launch {
            val result = usercase.invoke(email, password)
            result.collectLatest {
                when(it){
                    is FireBaseState.Loading -> {

                    }
                    is FireBaseState.Success -> {

                    }
                    is FireBaseState.Fail -> {

                    }
                }
            }
        }
    }
}
