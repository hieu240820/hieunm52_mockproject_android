package com.hieunguyen.personalfinanceapp.data.basenetwork

import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.tasks.await

class UserDataSourceImpl(private val mAuth: FirebaseAuth): UserDataSource {
    override suspend fun login(email: String, password: String): Boolean {
        return try {
            mAuth.createUserWithEmailAndPassword(email, password).await()
            true
        } catch (ex: FirebaseException) {
            false
        }
    }

    override suspend fun create(email: String, password: String): Boolean {
        return try {
            mAuth.signInWithEmailAndPassword(email, password).await()
            true
        } catch (ex: FirebaseException) {
            false
        }
    }
}