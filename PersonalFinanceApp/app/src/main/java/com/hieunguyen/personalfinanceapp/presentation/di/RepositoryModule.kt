package com.hieunguyen.personalfinanceapp.presentation.di

import com.hieunguyen.personalfinanceapp.data.basenetwork.UserDataSource
import com.hieunguyen.personalfinanceapp.data.repository.UserRepositoryImpl
import com.hieunguyen.personalfinanceapp.domain.repository.UserRepository
import com.hieunguyen.personalfinanceapp.domain.usecase.LoginUseCase
import com.hieunguyen.personalfinanceapp.domain.usecase.Usersusecase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideUserRepository(): UserRepository{
        return UserRepositoryImpl()
    }

    @Provides
    @Singleton
    fun provideUsersusecase(userRepository: UserRepository): Usersusecase
    {
        return Usersusecase(
            LoginUseCase(userRepository)
        )
    }
}